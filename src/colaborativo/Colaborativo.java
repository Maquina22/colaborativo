package Colaborativo;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author DANIEL
 */
public class Colaborativo {

    public static void main(String[] args) {
        int op = 0;
        do {
            try {
                op = Integer.parseInt(JOptionPane.showInputDialog(null,
                            "1. Saludar"
                        + "\n2. Conocer signo del Zodiaco (Cordoba Daniel)"
                        + "\n3. Conocer la edad de tu Perro  (Cardozo Franco)"
                        + "\n4. Adivinar un numero  (Torrez Miguel)"
                        + "\n5. Calcular Año Bisiesto (Farfan yamil)"            
                        + "\n99. Salir", "Menú", 3));
                switch (op) {
                    case 1://(Cordoba Daniel)
                        JOptionPane.showMessageDialog(null, "Hola");
                        break;
                    case 2:// (Cordoba Daniel)
                        signoZodiaco();
                        break;
                    case 3:// (Cardozo Franco)
                        edadPerro();
                        break;
                    case 4://(Torrez Miguel)   
                        Adivinar_numero();
                        break;
                    case 5:
                        calcularAnio();
                        break;
                    case 99:
                        JOptionPane.showMessageDialog(null, "Fin del Programa");
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Opcion no encontrada!");
                }
            } catch (NumberFormatException x) {
                JOptionPane.showMessageDialog(null, "Ingrese un numero!!!", "Error", 3);
            }
        } while (op != 99);
    }

    //Case 2 (Cordoba Daniel)
    public static void signoZodiaco() {
        int dia, mes;
        dia = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresar dia que naciste ", "Dia que naciste", 3));
        mes = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresar mes que naciste ", "Mes que naciste", 3));
        if (mes >= 1 && mes <= 12) {
            switch (mes) {
                case 1: //Enero
                    if (dia >= 1 && dia <= 31) {
                        if (dia >= 22) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Acuario"
                                    + "\n\nUn Acuario es simpatico y humanitario. Es honesto y totalmente leal, original"
                                    + "\ny brillante. Un Acuario es independiente e intelectual. Les gusta luchar por causas"
                                    + "\nbuenas, soñar y planificar para un futuro feliz, aprender del pasado, los buenos"
                                    + "\namigos y divertirse.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Capricornio"
                                    + "\n\nUn Capricornio tiene ambicion y es disciplinado. Es practico y prudente,"
                                    + "\ntiene paciencia y hasta es cauteloso cuando hace falta. Tiene un buen sentido"
                                    + "\nde humor y es reservado. A un capricornio le gusta la fiabilidad, el "
                                    + "\nprofecionalismo, una base solida, tener un objetivo, el liderazgo.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 2: //Febrero
                    if (dia >= 1 && dia <= 29) {
                        if (dia >= 19) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Piscis"
                                    + "\n\nUn Piscis es imaginativo y sensible, es amable y tiene compasion hacia"
                                    + "\nlos demas. Es intuitivo y piensa en los demas. Piscis tiene una personalidad"
                                    + "\ntranquila y tiene una gran capacidad creativa en el terreno de lo artistico.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Acuario"
                                    + "\n\nUn Acuario es simpatico y humanitario. Es honesto y totalmente leal, original"
                                    + "\ny brillante. Un Acuario es independiente e intelectual. Les gusta luchar por causas"
                                    + "\nbuenas, soñar y planificar para un futuro feliz, aprender del pasado, los buenos"
                                    + "\namigos y divertirse.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 3: //Marzo
                    if (dia >= 1 && dia <= 31) {
                        if (dia >= 21) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Aries"
                                    + "\n\nLos Aries son aventureros y energeticos, son pioneros y valientes."
                                    + "\nSon listos, dinamicos, seguros de si y suelen demostrar entusiasmo "
                                    + "\nhacia las cosas. Les gusta la aventura y los retos. Les gusta ganar"
                                    + "\ny son espontaneos. Tambien le gusta dar su apoyo a una buena causa.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Piscis"
                                    + "\n\nUn Piscis es imaginativo y sensible, es amable y tiene compasion hacia"
                                    + "\nlos demas. Es intuitivo y piensa en los demas. Piscis tiene una personalidad"
                                    + "\ntranquila y tiene una gran capacidad creativa en el terreno de lo artistico.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 4: //Abril
                    if (dia > 0 && dia <= 30) {
                        if (dia >= 22) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Tauro! "
                                    + "\n\nTauro es paciente, persistente, decidido y fiable. Le encanta"
                                    + "\nsentirse seguro. Tiene buen corazon y es muy cariñoso. A un Tauro"
                                    + "\nle gusta la estabilidad, las cosas naturales, el placer y la"
                                    + "\ncomodidad. Los tauro disfrutan con tiempo para reflexionar y les"
                                    + "\nencanta sentirse atraido hacia alguien.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Aries"
                                    + "\n\nLos Aries son aventureros y energeticos, son pioneros y valientes."
                                    + "\nSon listos, dinamicos, seguros de si y suelen demostrar entusiasmo "
                                    + "\nhacia las cosas. Les gusta la aventura y los retos. Les gusta ganar"
                                    + "\ny son espontaneos. Tambien le gusta dar su apoyo a una buena causa.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 5: //Mayo
                    if (dia > 0 && dia <= 31) {
                        if (dia >= 22) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Geminis"
                                    + "\n\nLos Geminis son intelectuales, elocuentes, cariñosos, comunicativos"
                                    + "\ne inteligentes. Tienen mucha energia y vitalidad. Les gusta hablar,"
                                    + "\nleer, hacer varias cosas a la vez. Los geminis disfrutan con lo inusual"
                                    + "\ny la novedad. cuanto mas variedad en su vida, mejor.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Tauro! "
                                    + "\n\nTauro es paciente, persistente, decidido y fiable. Le encanta"
                                    + "\nsentirse seguro. Tiene buen corazon y es muy cariñoso. A un Tauro"
                                    + "\nle gusta la estabilidad, las cosas naturales, el placer y la"
                                    + "\ncomodidad. Los tauro disfrutan con tiempo para reflexionar y les"
                                    + "\nencanta sentirse atraido hacia alguien.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 6: //Junio
                    if (dia > 0 && dia <= 30) {
                        if (dia >= 22) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Cancer"
                                    + "\n\nUn Cancer es emocional y cariñoso, protector y simpatico. Un cancer"
                                    + "\ntiene mucha imaginacion e intuicion y sabe ser cauteloso cuando hace "
                                    + "\nfalta. A un cancer le gusta su casa, el campo, los niños, disfrutar"
                                    + "\ncon sus aficiones y le gustan las fiestas.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Geminis"
                                    + "\n\nLos Geminis son intelectuales, elocuentes, cariñosos, comunicativos"
                                    + "\ne inteligentes. Tienen mucha energia y vitalidad. Les gusta hablar,"
                                    + "\nleer, hacer varias cosas a la vez. Los geminis disfrutan con lo inusual"
                                    + "\ny la novedad. cuanto mas variedad en su vida, mejor.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 7: //Julio
                    if (dia > 0 && dia <= 31) {
                        if (dia >= 22) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Leo"
                                    + "\n\nUn Leo es generoso y bondadoso, fiel y cariñoso. Un leo es creativo,"
                                    + "\nentusiasta y comprensivo con los demas. Le gusta la aventura, el lujo "
                                    + "\ny la comodidad. Un leo disfruta con los niños, el teatro y las fiestas."
                                    + "\nTambien le motiva el riesgo.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Cancer"
                                    + "\n\nUn Cancer es emocional y cariñoso, protector y simpatico. Un cancer"
                                    + "\ntiene mucha imaginacion e intuicion y sabe ser cauteloso cuando hace "
                                    + "\nfalta. A un cancer le gusta su casa, el campo, los niños, disfrutar"
                                    + "\ncon sus aficiones y le gustan las fiestas.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 8: //Agosto
                    if (dia > 0 && dia <= 31) {
                        if (dia >= 23) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Virgo"
                                    + "\n\nLos Virgos suelen ser meticulosos, practicos y trabajadores. Tienen"
                                    + "\ngran capacidad analistica y son fiables. A los virgos les gusta la vida"
                                    + "\nsana, hacer listas, el orden y la higiene.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Leo"
                                    + "\n\nUn Leo es generoso y bondadoso, fiel y cariñoso. Un leo es creativo,"
                                    + "\nentusiasta y comprensivo con los demas. Le gusta la aventura, el lujo "
                                    + "\ny la comodidad. Un leo disfruta con los niños, el teatro y las fiestas."
                                    + "\nTambien le motiva el riesgo.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 9: //Septiembre
                    if (dia > 0 && dia <= 30) {
                        if (dia >= 24) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Libra"
                                    + "\n\nUn Libra es diplomatico, encantador y sociable. Los libra son idealistas,"
                                    + "\npacificos, optimistas y romanticos. Tienen un caracter afable y equilibrado."
                                    + "\nLos libra se encuentran entre los signos mas civilizados del zodiaco.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Virgo"
                                    + "\n\nLos Virgos suelen ser meticulosos, practicos y trabajadores. Tienen"
                                    + "\ngran capacidad analistica y son fiables. A los virgos les gusta la vida"
                                    + "\nsana, hacer listas, el orden y la higiene.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 10: //Octubre
                    if (dia > 0 && dia <= 31) {
                        if (dia >= 22) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Escorpio"
                                    + "\n\nUn Escorpio es emocional, decidido, poderoso y apasionado. El Escorpio"
                                    + "\nes un signo con mucho magnetismo. A un Escorpio le gusta la verdad, el"
                                    + "\ntrabajo cuando tiene sentido, involucrarse en causas y convencer a los demas.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Libra"
                                    + "\n\nUn Libra es diplomatico, encantador y sociable. Los libra son idealistas,"
                                    + "\npacificos, optimistas y romanticos. Tienen un caracter afable y equilibrado."
                                    + "\nLos libra se encuentran entre los signos mas civilizados del zodiaco.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 11: //Noviembre
                    if (dia > 0 && dia <= 30) {
                        if (dia >= 23) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Sagitario"
                                    + "\n\nUn Sagitario es intelectual, honesto, sincero y simpatico. A los sagitario"
                                    + "\nles caracteriza el optimismo, su modestia y su buen humor. A los sagitario les"
                                    + "\ngusta la libertad, viajar, las leyes, la aventura. Es uno de los signos mas "
                                    + "\npositivos del zodiaco. Son versatiles, les encanta la aventura y lo desconocido.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Escorpio"
                                    + "\n\nUn Escorpio es emocional, decidido, poderoso y apasionado. El Escorpio"
                                    + "\nes un signo con mucho magnetismo. A un Escorpio le gusta la verdad, el"
                                    + "\ntrabajo cuando tiene sentido, involucrarse en causas y convencer a los demas.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
                case 12: //Diciembre
                    if (dia > 0 && dia <= 31) {
                        if (dia >= 22) {
                            JOptionPane.showMessageDialog(null, "Tu signo es Capricornio"
                                    + "\n\nUn Capricornio tiene ambicion y es disciplinado. Es practico y prudente,"
                                    + "\ntiene paciencia y hasta es cauteloso cuando hace falta. Tiene un buen sentido"
                                    + "\nde humor y es reservado. A un capricornio le gusta la fiabilidad, el "
                                    + "\nprofecionalismo, una base solida, tener un objetivo, el liderazgo.\n\n");
                        } else {
                            JOptionPane.showMessageDialog(null, "Tu signo es Sagitario"
                                    + "\n\nUn Sagitario es intelectual, honesto, sincero y simpatico. A los sagitario"
                                    + "\nles caracteriza el optimismo, su modestia y su buen humor. A los sagitario les"
                                    + "\ngusta la libertad, viajar, las leyes, la aventura. Es uno de los signos mas "
                                    + "\npositivos del zodiaco. Son versatiles, les encanta la aventura y lo desconocido.\n\n");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "El dia ingresado es incorrecto!", "Error", 3);
                    }
                    break;
            }
        } else {
            JOptionPane.showMessageDialog(null, "El mes ingresado es incorrecto!", "Error", 3);
        }
    }
    
    
    //Calcular la edad de tu perro realizado por Cardozo Franco
    public static void edadPerro(){
        int anio, tamañoraza;
        try {
            anio = Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresar cuantos años tiene tu perro (1-15) ", "Calculando años perros ha años humanos", 3));
            tamañoraza = Integer.parseInt(JOptionPane.showInputDialog(null, "1- Raza Grande \n"
                                                                        + "2- Raza Mediana \n"
                                                                        + "3- Raza Pequeña ", "Cual es la raza de su Perro", 3));
            int [][] matrizEdad = new int [15][3];
            matrizEdad[0][0]=16; matrizEdad[0][1]=16;matrizEdad[0][2]=16;
            matrizEdad[1][0]=24; matrizEdad[1][1]=24;matrizEdad[1][2]=24;
            matrizEdad[2][0]=29; matrizEdad[2][1]=29;matrizEdad[2][2]=29;
            matrizEdad[3][0]=34; matrizEdad[3][1]=34;matrizEdad[3][2]=34;
            matrizEdad[4][0]=39; matrizEdad[4][1]=39;matrizEdad[4][2]=39;
            matrizEdad[5][0]=46; matrizEdad[5][1]=45;matrizEdad[5][2]=43;
            matrizEdad[6][0]=53; matrizEdad[6][1]=51;matrizEdad[6][2]=47;
            matrizEdad[7][0]=60; matrizEdad[7][1]=57;matrizEdad[7][2]=51;
            matrizEdad[8][0]=67; matrizEdad[8][1]=63;matrizEdad[8][2]=55;
            matrizEdad[9][0]=74; matrizEdad[9][1]=69;matrizEdad[9][2]=59;
            matrizEdad[10][0]=81; matrizEdad[10][1]=75;matrizEdad[10][2]=63;
            matrizEdad[11][0]=88; matrizEdad[11][1]=81;matrizEdad[11][2]=67;
            matrizEdad[12][0]=95; matrizEdad[12][1]=87;matrizEdad[12][2]=72;
            matrizEdad[13][0]=102; matrizEdad[13][1]=93;matrizEdad[13][2]=76;
            matrizEdad[14][0]=109; matrizEdad[14][1]=98;matrizEdad[14][2]=80;

            JOptionPane.showMessageDialog(null, "La Edad de tu Perro en años Humanos es de "+matrizEdad[anio-1][tamañoraza-1]+" años ");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"La edad o la Raza Ingresada es incorrecta");
        }
    }
    
    //Case 4 (Torrez Miguel)
    public static void Adivinar_numero(){
        int n, num;
        // n es el número que hay que acertar
        // num guarda los números introducidos
        n=(int)(Math.random()*10)+1;
        // en lugar de pedir n... podemos hacer que se n tome un valor
        // aleatorio entre 1 y 10.
        // Así el juego es algo más entretenido.
        JOptionPane.showMessageDialog(null, "AHORA INTRODUCE UN NUMERO ENTRE EL 1 al 10");
        num = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce un numero "));
        while (num != n){ // mientras no coincidan ambos números
            if (num < n) {
                JOptionPane.showMessageDialog(null, "El numero es MENOR");
            } else {
                JOptionPane.showMessageDialog(null, "El numero es MAYOR");
            }
            num = Integer.parseInt(JOptionPane.showInputDialog(null, "Introduce otro numero "));
        }
        // al salir del mientras tenemos la certeza que num es igual a n

        JOptionPane.showMessageDialog(null,"acertaste...");
    }
    
    
    //Yamil Farfan 
    public static void calcularAnio(){
        int anio;
        
        anio=Integer.parseInt(JOptionPane.showInputDialog(null, "Ingresa el año mayor a 1000 "));
        if (anio > 1000){
            if (anio%4==0 && anio%100!=0 || anio%400==0 ){
                //imprimir un valor verdadero
                JOptionPane.showMessageDialog(null,"El año es biciesto");
            } else{ 
               JOptionPane.showMessageDialog(null ,"El año no es biciesto");
            }
        }else{
            JOptionPane.showMessageDialog(null, "El año Ingresado es Incorrecto");
        }
       
    }
}
                                                                                                                                                                                                                                                                                            